﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HotdogSwitcher : MonoBehaviour
{
    public GameObject hotdog;
    public AudioSource eatSound;
    public ParticleSystem assembleParticles;


    // Update is called once per frame
    public void SwitchHotdog()
    {
        StartCoroutine(SwitchHotdogCR());
    }

    IEnumerator SwitchHotdogCR()
    {
        hotdog.SetActive(false);
        Debug.Log("Disable");


        eatSound.Play();
        assembleParticles.Play();

        yield return new WaitForSeconds(.5f);

        hotdog.SetActive(true);
        Debug.Log("Enable");
    }
}
