﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class EnableRandomChild : MonoBehaviour
{
    private int randomNumber;
    

    // Start is called before the first frame update
    void Start()
    {
        Application.targetFrameRate = 30;


    }

    private void OnEnable()
    {

        List<GameObject> children = new List<GameObject>();
        int max = gameObject.transform.childCount;
        for (int i = 0; i < max; i++)
        {
            GameObject childToAdd = gameObject.transform.GetChild(i).gameObject;
            children.Add(childToAdd);
        }

        //Disable all
        foreach (GameObject child in children)
        {
            child.gameObject.SetActive(false);
        }

        //Random item
        randomNumber = Random.Range(0, children.Count);
        GameObject randomChild = children[randomNumber].gameObject;

        //Enable the one item to show
        randomChild.SetActive(true);
    }
    // Update is called once per frame
    void Update()
    {


    }
}
